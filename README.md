# VNSL3D

Cách chạy
Lấy source từ gitlab
```
git clone https://gitlab.com/hiephuynh132/vnsl3d.git
```

Lấy docker image từ hub
```
docker pull 18119148/vnsl:base
```

Mount environment với 
- _source_path: Đường dẫn của source lấy từ gitlab về
- _port: port available trên máy
```
docker run -it -d --mount type=bind,src=<_source_path>,dst=/home/ai/ -p <_port>:6000 18119148/vnsl:base
```

Chạy API
```
python3 API.py
```