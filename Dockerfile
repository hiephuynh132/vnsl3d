FROM ubuntu:22.04

WORKDIR /home/ai/

RUN apt-get update
RUN apt-get install python3 python3-pip git git-lfs -y
RUN apt-get install -y build-essential libboost-all-dev cmake zlib1g-dev libbz2-dev liblzma-dev
RUN apt-get install blender -y
RUN apt-get install libegl1-mesa -y
RUN apt-get install libsm6 libxrender1 libxext6 libglu1-mesa -y
RUN mkdir -p ~/.config/blender/4.2/scripts/addons/
COPY ./smplx_blender_addon ~/.config/blender/4.2/scripts/addons/
COPY ./requirements.txt /home/ai/requirements.txt
RUN pip install -r requirements.txt
EXPOSE 36000