import os
import shutil
from flask import Flask, request, Response, render_template
from speech2text import *
# from moviepy.editor import *
from translate import *
from underthesea import pos_tag
import subprocess
import datetime
import time
app = Flask(__name__,static_folder='static')

def search_dict(input_text):
    f = open("./rule/spell.txt",'r',encoding="UTF-8")
    spell_data = f.read()
    spell_data = spell_data.split("\n")
    f.close()
    video_id = [[],[]]
    f = open("./rule/label.txt",'r',encoding="UTF-8")
    label = f.read()
    f.close()
    for item in label.split("\n"):
        video_id[0].append(item.split("\t")[0])
        video_id[1].append(item.split("\t")[1])

    output = vie_to_sl_dict_processing(input_text)
    print(output)
    output_searching = pos_tag(output)
 
    Ls = []
    id_list = []
 
    for item in output_searching:
        for ele in item[0].lower():
            if ele in video_id[1]:
                id_character = video_id[0][video_id[1].index(ele)]
                id_list.append(id_character)
                # filePath = "./Blender_python_v6/" + id_character + ".mp4"
                # video = VideoFileClip(filePath)
                # Ls.append(video)
            else:
                str_sac = "áắấéếóốớíúứý"
                str_huyen = "àằầèềòồờìùừỳ"
                str_hoi = "ảẳẩẻểỏổởỉủửỷ"
                str_nga = "ãẵẫẽễõỗỡĩũữỹ"
                str_nang = "ạặậẹệọộợịụựỵ"
                str_ngang = "aăâeêoôơiuưy"
                if ele in str_sac:            
                    id_character = video_id[0][video_id[1].index(str_ngang[str_sac.index(ele)])]
                    id_list.append(id_character)
                    # id_character = video_id[0][video_id[1].index("dấu sắc")]
                    # id_list.append(id_character)
                elif ele in str_huyen:            
                    id_character = video_id[0][video_id[1].index(str_ngang[str_huyen.index(ele)])]
                    id_list.append(id_character)
                    # id_character = video_id[0][video_id[1].index("dấu huyền")]
                    # id_list.append(id_character)
                elif ele in str_hoi:            
                    id_character = video_id[0][video_id[1].index(str_ngang[str_hoi.index(ele)])]
                    id_list.append(id_character)
                    # id_character = video_id[0][video_id[1].index("dấu hỏi")]
                    # id_list.append(id_character)
                elif ele in str_nga:            
                    id_character = video_id[0][video_id[1].index(str_ngang[str_nga.index(ele)])]
                    id_list.append(id_character)
                    # id_character = video_id[0][video_id[1].index("dấu ngã")]
                    # id_list.append(id_character)
                elif ele in str_nang:            
                    id_character = video_id[0][video_id[1].index(str_ngang[str_nang.index(ele)])]
                    id_list.append(id_character)
                    # id_character = video_id[0][video_id[1].index("dấu nặng")]
                    # id_list.append(id_character)
    f = open("./id_list.txt",'w',encoding="UTF-8")
    for i in id_list:
        f.write(i + "\n")
    f.close() 
    # return id_list
global cnt
cnt = 0

@app.route('/',methods=['POST','GET'])
def home():
    global cnt
    if request.method == "POST":
        cnt += 1
        mode = request.form.get("mode")
        gender = request.form.get("gender")
        if mode == 'text':
            input_text = request.form.get("input")
            print(input_text)
            search_dict(input_text)
            
            if os.path.exists("./static/output"):
                shutil.rmtree("./static/output")
                os.makedirs("./static/output")
            if gender == "Female":
                subprocess.call('blender-4.2.0-linux-x64/blender -b -P create_blender_f_pkl.py', shell=True)
            else:
                subprocess.call('blender-4.2.0-linux-x64/blender -b -P create_blender_v2_pkl.py', shell=True)
            path = ""
            for i in os.listdir("./static/output"):
                if i.endswith(".mp4") and i != 'a.mp4':
                    now = datetime.datetime.now()
                    shutil.move("./static/output/" + i,"./static/output/" + str(now).replace(" ","").replace("-","").replace(":","").replace(".","") + ".mp4")
                    path = str(now).replace(" ","").replace("-","").replace(":","").replace(".","") + ".mp4"
            return path
        else:
            f = request.files['audio_data']
            with open('audio.wav', 'wb') as audio:
                f.save(audio)  
            start_time = time.time()
            output_s2t = speech_recognition("./audio.wav",processor,model,lm_file)
            search_dict(output_s2t)
            print(time.time() - start_time)
            if os.path.exists("./static/output"):
                shutil.rmtree("./static/output")
                os.makedirs("./static/output")
            subprocess.call('blender-4.2.0-linux-x64/blender -b -P create_blender_v2_pkl.py', shell=True)
            path = ""
            for i in os.listdir("./static/output"):
                if i.endswith(".mp4") and i != 'a.mp4':
                    now = datetime.datetime.now()
                    shutil.move("./static/output/" + i,"./static/output/" + str(now).replace(" ","").replace("-","").replace(":","").replace(".","") + ".mp4")
                    path = str(now).replace(" ","").replace("-","").replace(":","").replace(".","") + ".mp4"
            return path
            return path
        # print(request.form)
        # print(request.files)



        
        # mode = '1'
        # if mode == '2':
        #     search_dict(input_text)
        #     filepath="render.bat"
        #     if os.path.exists("./static/output"):
        #         shutil.rmtree("./static/output")
        #         os.makedirs("./static/output")
        #     os.system(filepath)
        #     path = ""
        #     for i in os.listdir("./static/output"):
        #         if i.endswith(".mp4") and i != 'a.mp4':
        #             now = datetime.datetime.now()
        #             shutil.move("./static/output/" + i,"./static/output/" + str(now).replace(" ","").replace("-","").replace(":","").replace(".","") + ".mp4")
        #             path = str(now).replace(" ","").replace("-","").replace(":","").replace(".","") + ".mp4"
        return render_template('home.html')
        return render_template('home.html',data=data)
    return render_template('home.html')
if __name__ == '__main__':
    cache_dir = './cache/'
    processor = Wav2Vec2Processor.from_pretrained("nguyenvulebinh/wav2vec2-base-vietnamese-250h", cache_dir=cache_dir)
    model = Wav2Vec2ForCTC.from_pretrained("nguyenvulebinh/wav2vec2-base-vietnamese-250h", cache_dir=cache_dir)
    lm_file = hf_bucket_url("nguyenvulebinh/wav2vec2-base-vietnamese-250h", filename='vi_lm_4grams.bin.zip')
    lm_file = cached_path(lm_file,cache_dir=cache_dir)
    with zipfile.ZipFile(lm_file, 'r') as zip_ref:
        zip_ref.extractall(cache_dir)
    lm_file = cache_dir + 'vi_lm_4grams.bin'
    
    app.run(host="0.0.0.0", port=6000)
    
