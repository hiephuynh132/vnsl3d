import bpy
import addon_utils
import shutil
import sys
import os
__dir__ = os.path.dirname(__file__)
sys.path.append(os.path.join(__dir__, ''))
from myPath import mypath
def enable_addon(addon_module_name):
    loaded_default, loaded_state = addon_utils.check(addon_module_name)
    if not loaded_state:
        addon_utils.enable(addon_module_name)

def add_keypoints(my_filepath,f):
    bpy.ops.object.smplx_load_pose(filepath=my_filepath)
    ob = bpy.data.objects["SMPLX-female"]
    for bone in bpy.data.objects["SMPLX-female"].data.bones:
        bone.select = True
        bpy.context.object.mode == "POSE"
        name = bone.name
        pb = ob.pose.bones.get(name) # None if no bone named name
        pb.keyframe_insert("location", frame=f)
        pb.keyframe_insert("rotation_quaternion", frame=f)
        pb.keyframe_insert("scale", frame=f)

def update_arm(my_filepath,f):
    bpy.ops.object.smplx_load_pose(filepath=my_filepath)
    ob = bpy.data.objects["SMPLX-female"]
    for bone in bpy.data.objects["SMPLX-female"].data.bones:
        bone.select = True
        bpy.context.object.mode == "POSE"
        name = bone.name
        bone_allow_list = ["collar","shoulder","elbow","wrist" ]
        if name.split("_")[-1] in bone_allow_list:
            pb = ob.pose.bones.get(name) # None if no bone named name
            pb.keyframe_insert("location", frame=f)
            pb.keyframe_insert("rotation_quaternion", frame=f)
            pb.keyframe_insert("scale", frame=f)
            
def update_hand(my_filepath,f):
    bpy.ops.object.smplx_load_pose(filepath=my_filepath)
    ob = bpy.data.objects["SMPLX-female"]
    for bone in bpy.data.objects["SMPLX-female"].data.bones:
        bone.select = True
        bpy.context.object.mode == "POSE"
        name = bone.name
        bone_allow_list = ["index1","index2","index3","middle1","middle2","middle3","pinky1","pinky2","pinky3",
                            "ring1","ring2","ring3","thumb1","thumb2","thumb3" ]
        if name.split("_")[-1] in bone_allow_list:
            pb = ob.pose.bones.get(name) # None if no bone named name
            pb.keyframe_insert("location", frame=f)
            pb.keyframe_insert("rotation_quaternion", frame=f)
            pb.keyframe_insert("scale", frame=f)
# bpy.ops.wm.addon_enable(module='~/.config/blender/3.0.1/scripts/addons/smplx_blender_addon')
# bpy.ops.wm.addon_enable(module="/root/.config/blender/3.0.1/scripts/addons/smplx_blender_addon")
enable_addon(addon_module_name="smplx_blender_addon")
data = {}
bpy.context.window_manager.smplx_tool.smplx_gender = 'female'
bpy.ops.scene.smplx_add_gender()
bpy.context.window_manager.smplx_tool.smplx_texture = 'smplx_texture_f_alb.png'
bpy.ops.object.smplx_set_texture()

frame_index = 0
total_sample = 0
# for item in os.listdir(mypath + "/input"):
f = open(mypath + "/id_list.txt",'r')
data_f = f.read()
f.close()
flag = 0

for item in data_f.split("\n")[:-1]:
    frame_step = 20
    if flag==0:
        frame_step = 2
        total_sample += 1
        add_keypoints(mypath + "/input/D0489/001.pkl",frame_index)
    for idx,ele in enumerate(sorted(os.listdir(mypath + "/input" + "/" + item))):
        if idx >= flag and idx <= 50:
            total_sample += frame_step
            path = "/input/" + item + "/" + ele
            if idx % 5 == 0:
                update_arm(mypath  + path,frame_index)
                update_hand(mypath  + path,frame_index)
            # else:
            #     update_hand(mypath  + path,frame_index)
            frame_index += frame_step
            if flag == 0 and idx == 40:
                break
            frame_step = 2
    flag = 22
frame_step = 5
for idx,ele in enumerate(sorted(os.listdir(mypath + "/input/D0489"))):
    if idx > 60 and idx < 80 and idx % 5 == 0:
        path = "/input/D0489/" + ele
        update_arm(mypath  + path,frame_index)
        update_hand(mypath  + path,frame_index)
        frame_index += frame_step
        total_sample += frame_step


for frame in range(0,total_sample):
    obj = bpy.data.objects['Camera'] # bpy.types.Camera
    obj.location.x = 0.0
    obj.location.y = 2.75
    obj.location.z = -0.8
    rx = 270
    ry = 0.0
    rz = 0.0
    pi = 3.14159265
    obj.rotation_euler[0] = rx*(pi/180.0)
    obj.rotation_euler[1] = ry*(pi/180.0)
    obj.rotation_euler[2] = rz*(pi/180.0)
    obj.keyframe_insert(data_path="location", frame=frame)
    obj.keyframe_insert(data_path="rotation_euler", frame=frame)
    obj = bpy.data.objects['Light']
    obj.location.x = 0.2
    obj.location.y = 1.5
    obj.location.z = -2
    obj.keyframe_insert(data_path="location", frame=frame)
    obj = bpy.data.lights['Light']
    obj.energy = 200.0    
    obj.keyframe_insert(data_path="energy", frame=frame)
        
bpy.context.scene.frame_start = 0
bpy.context.scene.frame_end = total_sample
try:
    object_to_delete = bpy.data.objects['Cube']
    bpy.data.objects.remove(object_to_delete, do_unlink=True)
except:
    pass
bpy.context.scene.render.image_settings.file_format = "FFMPEG"
bpy.context.scene.render.ffmpeg.format = "MPEG4"
bpy.context.scene.render.ffmpeg.codec = "H264"
bpy.context.scene.render.filepath = mypath + "/static/output/final_output_"
bpy.ops.render.render(animation=True,write_still=True)
# bpy.ops.wm.save_as_mainfile(filepath=mypath + "/static/output/final_output.blend")
        
