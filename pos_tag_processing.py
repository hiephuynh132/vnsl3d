from underthesea import pos_tag
def pos_tag_processing(data):
	national_words = ['V','N','A','M','R','E','C']
	intermediate_class = ['Np','Ns','P','Nc','CH']
	a = pos_tag(data)
	translated_tag = a
	translated = ""
	index = 0
	for text, tag in a:
		if (tag in national_words) == True or (tag in intermediate_class) == True : 
			translated += text +" "
		else: del translated_tag[index]
		index += 1
	translated = translated[0:len(translated)-1]
	return translated_tag


