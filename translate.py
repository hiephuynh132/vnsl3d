from pos_tag_processing import *
def vie_to_sl_dict_processing(sentence):
	f_question_word = open("./rule/question_word.txt",'r',encoding="UTF-8")
	question_word = f_question_word.read().split("\n")
	f_question_word.close()
	for question_w in question_word:
		if question_w in sentence:
			sentence += " ?"
			sentence = sentence.replace(question_w,"")
			break
	temp_sentence_tag = pos_tag_processing(sentence)
	f_no_meaning = open("./rule/no_meaning.txt",'r',encoding="UTF-8")
	no_meaning_word = f_no_meaning.read().split("\n")
	f_no_meaning.close()
	final_sentence_tag = temp_sentence_tag
	index = 0
	for text, tag in temp_sentence_tag:
		if (text in no_meaning_word) == True:
			del final_sentence_tag[index]
		index += 1
	final_sentence = ""
	list_sentence = []
	index = 0
	index_start = 0
	for text, tag in final_sentence_tag:
		if tag == 'CH' or tag == 'C':
			list_sentence.append(final_sentence_tag[index_start:index])
			index_start = index + 1
		else: final_sentence += text +" "
		index += 1
	if len(list_sentence) == 0: list_sentence.append(final_sentence_tag[index_start:index])
	rule_check = []
	rule = []
	for senten in list_sentence:
		f_rule = open("./rule/rule.txt",'r',encoding="UTF-8")
		for rule_line in f_rule.read().split("\n"):
			rule.append(rule_line.split("\t")[0])
			rule.append(rule_line.split("\t")[1])
		f_rule.close()
		if len(senten) >= 2:
			for word_num in range(2,4):
				index_start = 0
				while index_start + word_num <= len(senten):
					rule_check.append(senten[index_start:index_start+word_num])
					index_start += 1
	for sample in rule_check:		
		tag_sample = ""
		for text, tag in sample:
			if len(tag_sample) == 0: tag_sample += tag
			else: tag_sample += "+" + tag
		if tag_sample in rule : 	
			for replace_sample in sample:
				if replace_sample in final_sentence_tag:
					a_list = list(final_sentence_tag[final_sentence_tag.index(replace_sample)])
					a_list[1] = rule[rule.index(tag_sample)+1]
					updated_tuple = tuple(a_list)
					final_sentence_tag[final_sentence_tag.index(replace_sample)] = updated_tuple
	sl_dict = {
		'N' : 1,
		'CN' : 1,
		'M' : 2,
		'V' : 3,			
		'BN' : 4,
	}

	sl_check = ['N','CN','M','V','BN']
	for a_tuple in final_sentence_tag:
		a_list = list(a_tuple)
		if (a_list[1] in sl_check) == True:
			a_list[1] = sl_dict[a_list[1]]
			updated_tuple = tuple(a_list)			
			final_sentence_tag[final_sentence_tag.index(a_tuple)] = updated_tuple
		else:
			if a_list[1] == 'P':
				a_list[1] = 1
				updated_tuple = tuple(a_list)			
				final_sentence_tag[final_sentence_tag.index(a_tuple)] = updated_tuple
			elif a_list[1] == 'R':
				a_list[1] = 6
				updated_tuple = tuple(a_list)			
				final_sentence_tag[final_sentence_tag.index(a_tuple)] = updated_tuple
			else:
				a_list[1] = 5
				updated_tuple = tuple(a_list)			
				final_sentence_tag[final_sentence_tag.index(a_tuple)] = updated_tuple
	final_sentence_tag = sorted(final_sentence_tag, key=lambda x:x[1])
	final_sentence = ""
	for text, tag in final_sentence_tag:
		final_sentence += text +" "
	final_sentence = final_sentence.lower()
	if len(final_sentence) != 0:
		return final_sentence[0].upper() + final_sentence[1:]
	else: return ""