/*!
* Start Bootstrap - Stylish Portfolio v6.0.5 (https://startbootstrap.com/theme/stylish-portfolio)
* Copyright 2013-2022 Start Bootstrap
* Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-stylish-portfolio/blob/master/LICENSE)
*/
window.addEventListener('DOMContentLoaded', event => {

    const sidebarWrapper = document.getElementById('sidebar-wrapper');
    let scrollToTopVisible = false;
    // Closes the sidebar menu
    const menuToggle = document.body.querySelector('.menu-toggle');
    menuToggle.addEventListener('click', event => {
        event.preventDefault();
        sidebarWrapper.classList.toggle('active');
        _toggleMenuIcon();
        menuToggle.classList.toggle('active');
    })

    // Closes responsive menu when a scroll trigger link is clicked
    var scrollTriggerList = [].slice.call(document.querySelectorAll('#sidebar-wrapper .js-scroll-trigger'));
    scrollTriggerList.map(scrollTrigger => {
        scrollTrigger.addEventListener('click', () => {
            sidebarWrapper.classList.remove('active');
            menuToggle.classList.remove('active');
            _toggleMenuIcon();
        })
    });

    function _toggleMenuIcon() {
        const menuToggleBars = document.body.querySelector('.menu-toggle > .fa-bars');
        const menuToggleTimes = document.body.querySelector('.menu-toggle > .fa-xmark');
        if (menuToggleBars) {
            menuToggleBars.classList.remove('fa-bars');
            menuToggleBars.classList.add('fa-xmark');
        }
        if (menuToggleTimes) {
            menuToggleTimes.classList.remove('fa-xmark');
            menuToggleTimes.classList.add('fa-bars');
        }
    }

    // Scroll to top button appear
    document.addEventListener('scroll', () => {
        const scrollToTop = document.body.querySelector('.scroll-to-top');
        if (document.documentElement.scrollTop > 100) {
            if (!scrollToTopVisible) {
                fadeIn(scrollToTop);
                scrollToTopVisible = true;
            }
        } else {
            if (scrollToTopVisible) {
                fadeOut(scrollToTop);
                scrollToTopVisible = false;
            }
        }
    })
})

function fadeOut(el) {
    el.style.opacity = 1;
    (function fade() {
        if ((el.style.opacity -= .1) < 0) {
            el.style.display = "none";
        } else {
            requestAnimationFrame(fade);
        }
    })();
};

function fadeIn(el, display) {
    el.style.opacity = 0;
    el.style.display = display || "block";
    (function fade() {
        var val = parseFloat(el.style.opacity);
        if (!((val += .1) > 1)) {
            el.style.opacity = val;
            requestAnimationFrame(fade);
        }
    })();
};

const textarea = document.querySelector('textarea');
const initialHeight = 60;

const resize = () => {
	textarea.style.height = `${initialHeight}px`;
	const height = textarea.scrollHeight;
	textarea.style.height = `${height + initialHeight}px`;
};

resize();

textarea.addEventListener('input', resize);

var hideVideo_cnt = 0
if (hideVideo_cnt == 0)
    {
        var hideVideo = document.getElementsByClassName("hideVideo")[0];
        var hideVideo_title = document.getElementsByClassName("p_video")[0];
        hideVideo.style.display = "none";
        hideVideo_title.style.display = "block";
    }


function hideVideoFunction(){
    var hideVideo = document.getElementsByClassName("hideVideo")[0];
    var hideVideo_title = document.getElementsByClassName("p_video")[0];
    hideVideo_cnt += 1
    if (hideVideo_cnt > 0)
    {
        hideVideo.style.display = "block";
        hideVideo_title.style.display = "none";
    }
    else
    {
        hideVideo.style.display = "none";
        hideVideo_title.style.display = "block";
    }
    var myBarloading = document.getElementsByClassName("w3-light-grey")[0];    
    myBarloading.style.display = "none";
}

var hide_myBar_cnt = 0
if (hide_myBar_cnt == 0)
    {
        // var myBarloading = document.getElementById("loading");
        // myBarloading.hidden = "True";
        var myBarloading = document.getElementsByClassName("w3-light-grey")[0];
        myBarloading.style.display = "none";
    }
var flag = 0
function move() {
    var myBarloading = document.getElementsByClassName("w3-light-grey")[0];    
    myBarloading.style.display = "block";
    var textarea = document.getElementById('input')
    total_characters = textarea.value.length
    
    var elem = document.getElementById("myBar");
    var width = 20;
    var id = setInterval(frame, 1000*total_characters);
    
    function frame() {
      if (width >= 100 || flag==1) {
        if (width<100){
            width=100;
            elem.style.width = width + '%';
            elem.innerHTML = width * 1  + '%';
        }
        flag = 0
        clearInterval(id);
      } else {

        if (width < 98){
            width++;
            elem.style.width = width + '%';
            elem.innerHTML = width * 1  + '%';
        }
        // else {
        //     console.log(flag)
        //     if (flag == 1){
        //     console.log(flag)
        //     flag = 0
        //     console.log(flag)
        //     // setTimeout(() => {
        //     //     console.log("Delayed for 1 second.");    
        //     //   }, "1000")
        //     window.location.href = "http://127.0.0.1:5000/#about";
        //     location.reload();
        //     // var video = document.getElementById('video_tag');
        //     // var source = document.getElementById('source_tag');
        //     // video.pause()
        //     // source.setAttribute('src', "{{ url_for('static', filename='output/result.mp4') }}");
            
        //     // video.load();
            
        //     // video.play();
            
            
        //     }
        // }
      }
    }
  }

function setting_default(){
    var hideVideo = document.getElementsByClassName("hideVideo")[0];
    var hideVideo_title = document.getElementsByClassName("p_video")[0];
    hideVideo.style.display = "none";
    hideVideo_title.style.display = "block";
    path = "./static/output/a.mp4"
    var video = document.getElementById('video_tag');
    var source = document.getElementById('source_tag');
    source.setAttribute('src',"");
    video.load();
}